import * as productModel from '../model/product.model.js';
import fs from 'fs';

export const getAll = (req, res, next) => {
    const productList = productModel.getAll();
    res.json(productList);
}

export const create = (req, res, next) => {
    const product = JSON.parse(req.body.product);
    productModel.create({
        ...product,
        image: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`,
        user: req.token.email
    });
    res.status(201).json({ message: 'Produit créé' });
}

export const update = (req, res, next) => {
    const product = JSON.parse(req.body.product);
    const dataProduct = productModel.getOne(product.name);

    if (dataProduct.user === req.token.email) {
        const image = dataProduct.image.split('/');
        fs.unlinkSync(`images/${image[image.length - 1]}`);
        productModel.update({
            ...product,
            image: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`,
            user: req.token.email
        });
        res.status(201).json({ message: 'Produit modifié' });
    }
    else {
        res.status(403).json({ message: 'Vous ne pouvez pas faire ça' });
    }
}
