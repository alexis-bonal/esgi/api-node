import * as userModel from '../model/user.model.js';

export const getAll = (req, res, next) => {
    const userList = userModel.getAll();
    res.json(userList);
}