import express from 'express';
import * as productController from '../controller/product.controller.js';
import auth from '../middleware/auth.js';
import multer from '../middleware/multer.js';

const router = express.Router();

router.use(auth);

router.get('/', productController.getAll);
router.post('/', multer, productController.create);
router.put('/', multer, productController.update);

export default router;