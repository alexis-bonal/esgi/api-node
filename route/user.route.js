import express from 'express';
import * as userController from '../controller/user.controller.js';
import auth from '../middleware/auth.js';

const router = express.Router();

router.use(auth);

router.get('/profile', (req, res, next) => {
    res.json({ message: "profile" });
});

router.get('/', userController.getAll);

export default router;