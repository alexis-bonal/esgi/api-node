import express from 'express';
import authRoute from './route/auth.route.js';
import userRoute from './route/user.route.js';
import productRoute from './route/product.route.js';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();

app.use(express.json());

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});

app.use('/images', express.static(path.join(__dirname, "images")));
app.use('/user', userRoute);
app.use('/auth', authRoute);
app.use('/product', productRoute);

export default app;