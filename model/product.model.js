import productList from '../data/product.json' assert { type: "json" };
import fs from 'fs';

/**
 * Création d'un nouveau produit
 * @param {*} product Produit à créer
 */
export const create = (product) => {
    productList.push(product);
    fs.writeFileSync("data/product.json", JSON.stringify(productList, null, 4));
}

/**
 * Récupération de tous les produits
 * @returns Liste de tous les produits
 */
export const getAll = () => {
    return productList;
}

export const getOne = (name) => {
    const product = productList.find(product => product.name === name);
    if (product) {
        return product;
    } else {
        throw new Error("Produit non trouvé");
    }
}

export const update = (product) => {
    const productId = productList.findIndex(p => p.name === product.name);
    productList[productId] = product;
    fs.writeFileSync("data/product.json", JSON.stringify(productList, null, 4));
}