import http from 'http';
import app from './app.js';

const port = 3000;

const errorHandler = error => {
    console.error(error);
    process.exit(1);
}

const server = http.createServer(app);

server.on('error', errorHandler);
server.on('listening', () => {
    console.log('It works!');
});

server.listen(port);